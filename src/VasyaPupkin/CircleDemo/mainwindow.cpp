#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbRadius_clicked()
{
    QString r=ui->leRadius->text();
    radius=r.toDouble();
    ference=2*3.14159*radius;
    square=3.14159*radius*radius;
    ui->leFerence->setText(QString::number(ference));
    ui->leSquare->setText(QString::number(square));
}

void MainWindow::on_pbFerence_clicked()
{

}

void MainWindow::on_pbSquare_clicked()
{

}
