/*
1. Написать программу, которая запрашивает у пользователя пол,
рост и вес, а затем анализирует соотношение роста и веса, выда-
вая рекомендации к дальнейшим действиям (похудеть, потолстеть,
норма)
*/
#include <stdio.h>
#include <string.h>

void cleanIn() {
    char c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}
int main()
{
    char sex;
    int height = 0, weight = 0, idealWeight = 0;
    while (1) {
        printf("Enter your sex (M/F):\n");
        if (scanf("%c", &sex) == 1 && (sex == 'M' || sex == 'm' || sex == 'F' || sex == 'f'))
            break;
        else {
            puts("Input error!");
            cleanIn();
        }
    }
    while (1) {
        printf("Enter your height (decimal number in centimeters):\n");
        if (scanf("%d", &height) == 1 && (height > 0 && height < 300))
            break;
        else {
            puts("Input error!");
            cleanIn();
        }
    }
    while (1) {
        printf("Enter your weight (decimal number in kilos):\n");
        if (scanf("%d", &weight) == 1 && (weight > 0 && weight < 800))
            break;
        else {
            puts("Input error!");
            cleanIn();
        }
    }

    if (sex == 'M' || sex == 'm') {
        idealWeight = height - 100;
    }
    else {
        idealWeight = height - 110;
    }

	if (weight == idealWeight)
		printf("You weight is normal.\n");
	else if (weight > idealWeight)
		printf("You need to lose some weight.\n");
	else
		printf("You need to increase your weight.\n");
	return 0;
}